/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastropessoas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 *
 * @author arthu_qf7f61o
 */
public class CadastroPessoas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        
        BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
        int escolha;
        
        ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
        
        while(true){
        
            System.out.println("##Bosstrategy##");
            System.out.println("Cadastro de pessoas");
            System.out.println("Escolha a sua opção:");
            System.out.println("1 - Cadastrar nova pessoa");
            System.out.println("2 - Editar velha pessoa");
            System.out.println("3 - Excluir pessoa");
            System.out.println("4 - Listar pessoas");

            escolha = Integer.parseInt(teclado.readLine());

            switch (escolha) {
                case 1:
                    pessoas.add(Cadastrar());

                    break;
                case 2:
                    Listar(pessoas);
                    System.out.println("Indique a posição da pessoa a ser editada: ");
                    escolha = Integer.parseInt(teclado.readLine());
                    Pessoa velhaPessoa = pessoas.get(escolha);

                    pessoas.set(escolha, Editar(velhaPessoa));

                    Listar(pessoas);
                    break;
                case 3:
                    Listar(pessoas);
                    System.out.println("Indique a posição da pessoa a ser excluida: ");
                    escolha = Integer.parseInt(teclado.readLine());

                    pessoas.remove(escolha);
                    break;
                case 4:
                    Listar(pessoas);
                    break;
                default:
                    throw new AssertionError();
            }
        }
    }
    
    static Pessoa Cadastrar() throws IOException{
        BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
        
        Pessoa novaPessoa = new Pessoa();
        System.out.println("Insira o nome da pessoa: ");
        novaPessoa.setNome(teclado.readLine());
        System.out.println("Insira a idade da pessoa: ");
        novaPessoa.setIdade(Integer.parseInt(teclado.readLine()));
        System.out.println("Insira o sexo da pessoa: ");
        novaPessoa.setSexo(teclado.readLine());
        System.out.println("Insira o CPF da pessoa: ");
        novaPessoa.setCpf(teclado.readLine());
        System.out.println("Insira o saldo da pessoa: ");
        novaPessoa.setSaldo(Double.parseDouble(teclado.readLine()));
        
        return novaPessoa;
    }
    
    static void Listar(ArrayList<Pessoa> pessoas) {
        int i = 0;
        for (Pessoa pessoa: pessoas) {
            System.out.println("Posição: " + i + " nome: " + pessoa.getNome() + ""
                    + " idade: " + pessoa.getIdade() + " sexo: " + pessoa.getSexo() + ""
                    + " cpf: " + pessoa.getCpf() + " saldo: " + pessoa.getSaldo());
            i++;
        }
    }
    
    static Pessoa Editar(Pessoa velhaPessoa) throws IOException{
        BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.println("nome: " + velhaPessoa.getNome());
        System.out.println("Insira o nome da pessoa: ");
        velhaPessoa.setNome(teclado.readLine());

        System.out.println("Idade: " + velhaPessoa.getIdade());
        System.out.println("Insira a idade da pessoa: ");
        velhaPessoa.setIdade(Integer.parseInt(teclado.readLine()));

        System.out.println("Sexo: " + velhaPessoa.getSexo());
        System.out.println("Insira o sexo da pessoa: ");
        velhaPessoa.setSexo(teclado.readLine());

        System.out.println("CPF: " + velhaPessoa.getCpf());
        System.out.println("Insira o CPF da pessoa: ");
        velhaPessoa.setCpf(teclado.readLine());

        System.out.println("Saldo: " + velhaPessoa.getSaldo());
        System.out.println("Insira o saldo da pessoa: ");
        velhaPessoa.setSaldo(Double.parseDouble(teclado.readLine()));
        
        return velhaPessoa;
    }
    
}
